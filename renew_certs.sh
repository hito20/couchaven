#!/bin/bash
docker run --rm -it -v $(pwd)/configs/certs/:/etc/letsencrypt/ --volumes-from=couchaven_nginx_1 carlospac/arm32v7-certbot renew -w /well_known/ --webroot
