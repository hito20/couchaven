server {
    server_name example.com;
    listen 80;

    location /.well-known/ {
        root /well_known/;
    }
}

server {
    listen 90;

    location /radarr/ {
        proxy_pass http://radarr:7878/radarr/;
    }
    location /sonarr/ {
        proxy_pass http://sonarr:8989/sonarr/;
    }
    location /transmission/ {
        proxy_pass http://transmission:9091/transmission/;
    }
}

upstream radarr_backend {
    server radarr:7878;
}

upstream sonarr_backend {
    server sonarr:8989;
}

upstream transmission_backend {
    server transmission:9091;
}

server {
    listen 443 ssl;
    server_name example.com;
    access_log  /var/log/nginx/pi3.log;
    error_log  /var/log/nginx/pi3-errors.log;
    underscores_in_headers on;

    client_max_body_size 10M;

    ssl_protocols TLSv1.1 TLSv1.2;
    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;

    location /radarr/ {
        proxy_pass http://radarr_backend/radarr/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect     off;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
    }

    location /sonarr/ {
        proxy_pass http://sonarr_backend/sonarr/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect     off;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
    }

    location /transmission/ {
        proxy_pass http://transmission_backend/transmission/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect     off;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
        proxy_pass_header   X-Transmission-Session-Id;
    }
}
