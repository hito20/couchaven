# Home RPi 3 Cinema Setup with Sonarr and Radarr

## Description

Home Cinema setup in a Raspberry Pi 3 B+. Tested with OSMC (raspbian) with Sonarr and Radarr to manage your TV Series and movies, all under a docker enviroment for a faster and easy installation/migration.

## Install

On a clean raspbian/OSMC install just run `startup.sh`, this will install and setup docker for user `osmc`, edit as needed.

## Services

- Sonarr
- Radarr
- Transmission
- Nginx with SSL (Optional)

## TODO

- schedule certificate renewal
- automatic `/etc/fstab`
- setup autostart
- make nginx wait for other services to start OR force periodic reload
- test auto-restarting containers
- point volumes to a mounted drive
- prompt for users and pw's in the startup script

## Misc commands

### NTFS notice

If nginx is serving content from an NTFS drive, be careful with the permissions, you might need to set them more open than desired. In alternative attempt to add noserverino to the fstab

### Swap

```bash
sudo dd if=/dev/zero of=/swapfile1 bs=1G count=1
sudo chmod 0600 /swapfile1
sudo mkswap /swapfile1
sudo swapon /swapfile1
```
```sh
fallocate -l 4G /swapfile
dd if=/dev/zero of=/swapfile count=4096 bs=1MiB
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
swapon --show
free -h
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
```

### List services

`systemctl list-unit-files`

### Fstab setup

**Get HDD ID**

`blkid`

**/etc/fstab**

umask=007 is actually the same as chmod 770, making others not able to read/list files, useful for nextcloud

```bash
UUID=949ABD919ABD707A /mnt ntfs nofail,defaults,umask=0770 0 0
UUID=949ABD919ABD707A /mnt ntfs-3g nofail,defaults,uid=1000,gid=1000,umask=007 0 0
```

**Test fstab**

`mount -a`

### SSL

Before generating the certificates comment the following lines in `configs/nginx/home.conf`

```
    # listen 443 ssl;

    # ssl_protocols TLSv1.1 TLSv1.2;
    # ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    # ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
```

This is due to the fact that the certificates do not exist yet.

Next step you can perform a dry-run to generate the certificates:
*Note*: Replace `volumes-from=nginx_1` for the correct container name, you can check it with `docker ps`

- ### Certonly

```bash
docker run --rm -it -v $(pwd)/configs/certs/:/etc/letsencrypt/ --volumes-from=nginx_1 carlospac/arm32v7-certbot -t certonly --webroot -w /well_known/ -d your.domain.here --dry-run -m test@email.com --agree-tos
```

If it is successful remove the `--dry-run` option and run the command again, it should make the certificates available at `configs/certs`

- ### Renew

```bash
docker run --rm -it -v $(pwd)/configs/certs/:/etc/letsencrypt/ --volumes-from=nginx_1 carlospac/arm32v7-certbot renew -w /well_known/ --dry-run --webroot
```

- ### List Certificates

```bash
docker run --rm -it -v $(pwd)/configs/certs/:/etc/letsencrypt/ --volumes-from=nginx_1 carlospac/arm32v7-certbot certificates
```
