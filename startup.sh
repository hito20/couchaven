#!/bin/bash

echo "1 - Install Dependencies"

apt-get install apt-transport-https ca-certificates software-properties-common -y 

echo "2 - Downloading and installing Docker"

curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh

echo "3 - Configure user to run docker without sudo"

groupadd docker
usermod -aG docker osmc

echo "4 - Configure repo for updates"

curl https://download.docker.com/linux/raspbian/gpg | sudo apt-key add -
if grep -q "docker" /etc/apt/sources.list; then
    echo "/etc/apt/sources.list configured"
else
    echo "deb https://download.docker.com/linux/raspbian/ stretch stable" | tee -a /etc/apt/sources.list
fi
apt update # (warnings may appear)

echo "5 - Install docker-compose from PyPi"

apt-get install python3-pip python3-setuptools python3-dev gcc make libffi-dev libssl-dev git -y
pip3 install wheel
pip3 install docker-compose
cp /home/osmc/.local/bin/docker-compose /usr/local/bin/docker-compose

# echo "Extras - htop, glances" -> Beware this makes docker-compose not functional

# pip3 install docker-py
# apt install htop # glances
# 7 - Generate ssh key

# ssh-keygen -o -t rsa -b 4096 -C "email@example.com"